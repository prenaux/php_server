#!/bin/bash
echo "Stop Nginx..."
taskkill -f -IM nginx.exe
echo "Stop PHP-CGI..."
taskkill -f -IM php-cgi.exe
exit 1
