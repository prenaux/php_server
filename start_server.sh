#!/bin/bash
./stop_server.sh
echo "Starting Nginx..."
nginx &
echo "Starting PHP-CGI..."
php-cgi -c ./conf -b 127.0.0.1:9123 &
